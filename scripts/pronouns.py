import csv
import json

output = {}

with open("pronouns.csv") as file:
    reader = csv.reader(file, delimiter=',')
    line_count = 0
    read_pronouns = []
    for row in reader:
        if line_count % 10 == 0:
            line_count = 0
        line_count += 1
        if line_count == 1:
            current_set = row[line_count-1]
            output[current_set] = []
        col_count = 0
        for pronoun in row:
            col_count += 1
            if pronoun not in read_pronouns:
                output[current_set].append({"latin": pronoun, "parts": []})
                
            pronoun_parts = [p for p in output[current_set] if p['latin'] == pronoun][0]["parts"]
            read_pronouns.append(pronoun)
            
            if line_count == 1 or line_count == 6:
                case = "nom"
            elif line_count == 2 or line_count == 7:
                case = "acc"
            elif line_count == 3 or line_count == 8:
                case = "gen"
            elif line_count == 4 or line_count == 9:
                case = "dat"
            elif line_count == 5 or line_count == 10:
                case = "abl"

            if col_count == 1:
                gender = "m"
            elif col_count == 2:
                gender = "f"
            elif col_count == 3:
                gender = "n"

            if line_count < 6:
                number = "s"
            elif line_count > 5:
                number = "p"
            
            pronoun_parts.append({"number": number, "gender": gender, "case": case})

print(json.dumps(output))
