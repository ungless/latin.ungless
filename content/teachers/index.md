---
title: Teachers
---
# Do you teach Latin at GCSE or A Level?
I get up to 1,000 pupils using this website at peak exam season. I earn no money from this website.

If you would like to contribute your time to this common resource, open to all, here are some ways you can help:

1. Contribute translations of set texts.
2. Contribute annotations of set texts.
3. Contribute missing vocabulary.
4. Share the link to this website if you find it useful!

If you would like to donate some of your time, get in touch with me at max(at)ungless.net and I will let you know what to do.

I will credit literature donations, but note they will placed in the public domain, not copyrighted.

# Do you study GCSE or A Level Latin, and would like to contribute?

The same advice applies. Email me and let me know how you might like to help. All is appreciated.
